export const Stats = ({ removed, added }) => {
  return (
    <div className='stats'>
      <p>
        Удалено: {removed}; Добавлено: {added}
      </p>
    </div>
  );
};
