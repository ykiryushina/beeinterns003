export const UserButton = ({ id, selected, name, addFunc }) => {
  return (
    <button onClick={() => addFunc(id)} className={selected && 'disabled'}>
      {name}
    </button>
  );
};
