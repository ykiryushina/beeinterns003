export const Card = ({ name, phone, website, email, company, removeFunc, id }) => {
  return (
    <div className='card'>
      <h1>{name}</h1>
      <p>
        Phone: <span>{phone}</span>
      </p>
      <p>
        WebSite: <span>{website}</span>
      </p>
      <p>
        Email: <span>{email}</span>
      </p>
      <p>
        Company: <span>{company.name}</span>
      </p>
      <button onClick={() => removeFunc(id)}>Удалить из списка</button>
    </div>
  );
};
