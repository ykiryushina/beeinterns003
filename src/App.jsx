import { useState, useEffect } from 'react';
import { UserButton } from './components/user';
import { Stats } from './components/stats';
import { Card } from './components/card';

function App() {
  const [users, setUsers] = useState(null);
  const [addCounter, setAddCounter] = useState(0);
  const [removeCounter, setRemoveCounter] = useState(0);

  const apiGetUsers = () => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((json) => setUsers(json));
  };

  const addListElement = (id) => {
    const user = users.find((el) => el.id === id);
    if (user.selected) {
      return false;
    }
    const newUsers = users.map((el) => {
      if (el.id === id) {
        el.selected = true;
      }
      return el;
    });
    setUsers(newUsers);
    setAddCounter((prev) => prev + 1);
  };
  const removeListElement = (id) => {
    const user = users.find((el) => el.id === id);
    if (!user.selected) {
      return false;
    }
    const newUsers = users.map((el) => {
      if (el.id === id) {
        el.selected = false;
      }
      return el;
    });
    setUsers(newUsers);
    setRemoveCounter((prev) => prev + 1);
  };

  const getSelected = () => {
    if (!users) {
      return false;
    }
    if (users.filter((el) => el.selected).length === 0) {
      return false;
    }
    return users.filter((el) => el.selected);
  };

  useEffect(() => {
    apiGetUsers();
  }, []);

  console.log(users);
  return (
    <div className='container'>
      <div className='cards'>
        {getSelected() ? (
          getSelected().map((user, i) => (
            <Card {...user} removeFunc={removeListElement} key={user.id + i} />
          ))
        ) : (
          <h1>У вас нет пользователей в списке</h1>
        )}
      </div>
      {(removeCounter > 0 || addCounter > 0) && (
        <Stats removed={removeCounter} added={addCounter} />
      )}
      <div className='buttons'>
        {users &&
          users.map((user, i) => (
            <UserButton
              key={user.id + i}
              id={user.id}
              selected={user.selected}
              name={user.name}
              addFunc={addListElement}
            />
          ))}
      </div>
    </div>
  );
}

export default App;
